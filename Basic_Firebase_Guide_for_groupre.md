# Setting up Firebase!
1) [Go here and follow steps #1-#2 to download the `key.json` file that you need](https://cloud.google.com/community/tutorials/building-flask-api-with-cloud-firestore-and-deploying-to-cloud-run)

**Important Note**: On step `1a` of the link above, where it says to click on `Database`, do not listen to the guide and, instead, click on `Realtime Database`.
2) [Then go here and follow steps #1-#3 to download the `fbConfig.json` file that you need](https://github.com/cappp/dbm-firebase#where-find-the-x-values-of-my-firebase-project)

**Important Note**: When you're on the step about creating the app as you follow the second link, make it a **web app** and do not check the box that talks about Firebase hosting.

# Creating separate instances of one Firebase for separate development

You have to create another Firebase project strictly for your own development (**you should already have your own Firebase project if you followed those 2 links above for `key.json` and `fbConfig.json`**). Then export the data from the production database and import it into your developmental database. Google has released a guide on how to export/import backups [here](https://firebase.google.com/docs/database/backups).


# How to use Firebase on Flask
After you have `key.json` and `fbConfig.json`, the base source code that initializes Firebase can be found in `database.py`. Below are example snippets of how to carry out common functions available with Firebase on Flask.
# Creating Users
```python
email = input()
password = input()

try:
    #Try creating the user account using the provided data
    auth.create_user_with_email_and_password(email, password)
except Exception as e:
    #Print the error if something went wrong with registration
    print(e)
```
# Logging in Users
```python
email = input()
password = input()

try:
    #Login the user
    user = auth.sign_in_with_email_and_password(email, password)
except Exception as e:
    #Print error if user could not login
    print(e)
```
Note that `user` is now a dictionary with retrievable values. Some of the keys are `email` and `localId`. This means you can use code like the following and store information as [session](https://flask.palletsprojects.com/en/1.1.x/api/#flask.session) variables (you should've already imported `session` from earlier):
```python
session['user_email'] = user['email']
session['user_id'] = user['localId']
```

`session` is useful because it allows us to store different values for the same keys across users.
# Setting/Retrieving values in Firebase
The variable `db` should already be defined from early on in the guide.
```python
#Setting
  db.child("users").set(data)

#Updating
  #Updating is not the same as setting a variable.
  #If the firebase object you want to modify is a list, then setting will clear that entire list instead of modifying the specific part that you desire.
  #Update will only change the part that you desire.
  db.child("users").update(some_variable)

#Retrieving
  #The code below is an example of how 
  # to retrieve a table (but it doesn't necessarily 
  # have to be a table)
  data = db.child("users").get()

```

