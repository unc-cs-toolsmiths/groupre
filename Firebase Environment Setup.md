# Setting up The Environment Variables for Login/Sign-up

You're gonna need to set 2 keys in the environment variables within Gitlab:
- `FIREBASE_CONFIG`
- `FIREBASE_KEY`

`FIREBASE_CONFIG` is the key that can be found under the "General" tab of PROJECT SETTINGS (it should be found near the end of the page).

`FIREBASE_KEY` is the key that you can download from the PROJECT SETTINGS in Firebase (it's the service account).

# Creating separate instances of one Firebase for separate development

You have to create another Firebase project strictly for your own development. Then export the data from the production database and import it into your developmental database. Google has released a guide on how to export/import backups [here](https://firebase.google.com/docs/database/backups).