# groupre

Welcome to the groupre GitHub repository!

## Overview

Groupre is a program written in python meant to allow fast, automated matching of students to groups and chairs based on student preferences, professor settings, and chair attributes.

As a result of being created within the [Software Engineering Lab][COMP 523] course at the [University of North Carolina at Chapel Hill][UNC-CH], this repository contains the groupre python source code as well as the web implementation for professor use of it at UNC-CH. The current web-app locations can be found at our website [groupre.cs.unc.edu][website].

## Documentation

Documentation for the groupre project can be found [here](docs), with archives located [here](docs/archive).

## Usage

### Prerequisites

* [Python 3.7][Python3]
* [Flask][Flask]

To install the prerequisites, run `pip3 install <name>`, where `<name` might be
`Flask` or anything in the `requirements.txt` file. If you've
already installed Python2 versions of these dependencies with `pip`, you should
run `pip uninstall <name>` first; otherwise, you may get seemingly unrelated
error messages when python2 tries to execute python3 code.

### General Information

Prospective users looking to utilize the groupre python module for a different organization are encouraged to pull the groupre module source code located [here][masterSrc].

Here are the currently available command-line flags you can use when calling groupre:

* **-c \<CHAIRS>** or **--chairs \<CHAIRS>**
  * The chairs input file.
* **-s \<STUDENTS>** or **--students \<STUDENTS>**
  * The students input file.
* **-s \<ROSTER>** or **--roster \<ROSTER>**
  * The roster input file.
* **-o \<OUTPUT>** or **--output \<OUTPUT>**
  * The output file.

### Preparing Files
Follow the document here if you need help working with the files look at the how to prepare the files section.
[here](docs/Groupre_Instruction.pdf)

### Running Groupre cli

You can use the groupre module directly via the command line by entering the following:

```bash
groupre.py -c <CHAIRS> -s <STUDENT_PREFERENCES> -r <ROSTER> -o <OUTPUT>
```

Where _\<CHAIRS>_, _\<STUDENT_PREFERENCES>_, _\<ROSTER> and _\<OUTPUT>_ are file locations for those respective files.

Note: If groupre.py has not been given execution permissions, you may need to preface this command with "python" or your machine's equivalent Python 3 alias.

### Running Groupre webapp locally

After all prerequisites are installed, go into the "wsgi.py" file and change the variable "temp_file" to point directly to the folder where groupre is located.
Now, in the root folder of the app run command:
```bash
python3 wsgi.py
```
## Development

## Updating server

Look into this doc on how to update the website server manually.
[here](docs/VM_Guide.pdf)

## Developing with Flask

Developers looking to modify html files need to look in the *templates* directory. All other relevant files can be found (and should be stored) in the *static* directory.

## Tests

There are Python unit tests for the backend and Cypress browser tests for the frontend.

To run the backend tests:

```bash
cd unittest
python3 -m unittest discover
```

To run the frontend tests, first start the server:

```
npm run start-server
```

Then, in a separate terminal window:

```
npm test
```

If all is well, the specs should pass. You may see changes to a couple of files
in the `uploads/` directory which have to do with line endings; these changes
can be ignored and reverted with `git restore <filename>` (assuming a
relatively recent version of git with the `restore` command available).

When you're finished running tests, you can kill the backend server process
with control+c (assuming you don't want to keep it around to develop against).

### Troubleshooting

If when running `npm test` you see output in the terminal window running the
backend that looks like this:

```
[2020-05-07 11:37:42,203] ERROR in app: Exception on /room-saver [POST]
Traceback (most recent call last):
  File "/usr/local/lib/python2.7/site-packages/flask/app.py", line 2446, in wsgi_app
    response = self.full_dispatch_request()
  File "/usr/local/lib/python2.7/site-packages/flask/app.py", line 1951, in full_dispatch_request
    rv = self.handle_user_exception(e)
  File "/usr/local/lib/python2.7/site-packages/flask/app.py", line 1820, in handle_user_exception
    reraise(exc_type, exc_value, tb)
  File "/usr/local/lib/python2.7/site-packages/flask/app.py", line 1949, in full_dispatch_request
    rv = self.dispatch_request()
  File "/usr/local/lib/python2.7/site-packages/flask/app.py", line 1935, in dispatch_request
    return self.view_functions[rule.endpoint](**req.view_args)
  File "/Users/jeff/code/toolsmiths/groupre/wsgi.py", line 350, in saveRoom
    with open(filepath, 'w', newline='') as csvfile:
TypeError: 'newline' is an invalid keyword argument for this function
/Users/jeff/code/toolsmiths/groupre/uploads/chairs/room-test-room1-10-10.csv
```

...that will probably cause the corresponding tests to fail, and it means that
you're using python2 instead of python3.
See the prerequisites section above, but essentially you'll need to `pip
uninstall <name>` for each package in `requirements.txt` followed by `pip3
install <name>` for the same packages.

### Test Coverage

You can measure how well the backend code is covered by the tests. To do so, first install a Python package:

```bash
pip install coverage
```

Then to check test coverage:

```bash
cd unittest
coverage run -m unittest discover
coverage report -m
```

## Built With

* [Flask][Flask] - The web framework used for our Carolina CloudApps deployment.
* [gunicorn][gunicorn] - A Python WSGI HTTP Server for UNIX.
* This webapp is hosted on UNC Computer Science department's VM server using nginx

## Contributing

View our [CONTRIBUTING.md][contributing_file] file for details.

## Versioning

An official versioning template has not yet been chosen.

## License

View our chosen [LICENSE][license_file] file for details.

<!-- Begin References -->
[UNC-CH]: https://www.unc.edu/
[COMP 523]: https://wwwx.cs.unc.edu/Courses/comp523-f17/deliverables.php
[masterSrc]: https://github.com/jeyerena/ClassTeamBuilder/tree/master/src/groupre
[masterDocs]: https://github.com/jeyerena/ClassTeamBuilder/tree/master/docs
[masterDocsArchive]: https://github.com/jeyerena/ClassTeamBuilder/tree/master/docs/archive
[developDocs]: https://github.com/jeyerena/ClassTeamBuilder/tree/develop/docs
[developDocsArchive]: https://github.com/jeyerena/ClassTeamBuilder/tree/develop/docs/archive
[Flask]: http://flask.pocoo.org/
[contributing_file]: https://github.com/jeyerena/ClassTeamBuilder/tree/master/CONTRIBUTING.md
[license_file]: https://github.com/jeyerena/ClassTeamBuilder/blob/master/LICENSE
[Python3]: https://www.python.org/downloads/
[website]: http://groupre.cs.unc.edu
<!-- End References -->
