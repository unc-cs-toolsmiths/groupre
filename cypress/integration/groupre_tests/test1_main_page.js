describe('Testing Main page', () => {
  it('visits the Groupre website', () => {
    cy.visit("/")
  })

  it('returns file after "run" clicked', () => {
    // https://www.npmjs.com/package/cypress-file-upload
    // used for file upload
    cy.visit("/")
    const pref_path = "inputs/student.csv";
    const chair_path = "inputs/chair.csv";
    const roster_path = "inputs/roster.csv";
    cy.get("#pref_input").attachFile(pref_path)
    cy.get("#chair_input").attachFile(chair_path)
    cy.get("#roster_input").attachFile(roster_path)
    cy.get("#run").click()
    cy.url().should('eq', Cypress.config().baseUrl + '/')
    // const exist_cmd = Cypress.platform === 'win32' ? "if exist ..\\..\\Downloads\\output.csv echo It exists!" : "[ -f ~/Downloads/output.csv ] && echo 'It exists!'"
    // cy.exec(exist_cmd).its('stdout').should('contain', 'It exists')
    // const delete_cmd = Cypress.platform === "win32" ? "del ..\\..\\Downloads\\output.csv" : "rm ~/Downloads/output.csv"
    // cy.exec(delete_cmd)
  })

  it('contains previous test input file in local storage', () => {
    // cy.visit("/")
    // visit refreshs local storage, need this figure this test out before merge.
    // cy.contains("student.csv , chair.csv , roster.csv")
  })

  it('contains all the navigation', () => {
    cy.visit("/")
    cy.contains("Home Page")
    cy.contains("Build Class")
    cy.contains("Edit Class")
    cy.contains("Build Team")
    cy.contains("Edit Team")
    //cy.get("body").find("img").should("have.attr","src").should("include","../static/gitlab.png")
  })

  it('changes route to / when "Home" is clicked', () => {
    cy.visit("/")
    // force:true allows test to click on object thats not in the viewport, ignores the viewport error
    cy.get("#homePage").click({force: true})
    cy.url().should('eq', Cypress.config().baseUrl + '/') // => true
  })

  it('changes route to room-creation when "Build Class" is clicked', () => {
    cy.visit("/")
    cy.contains("Build Class").click({force: true})
    cy.url().should('eq', Cypress.config().baseUrl + '/room-creation') // => true
  })

  it('changes route to editTemplate when "Edit Class" is clicked', () => {
    cy.visit("/")
    cy.contains("Edit Class").click({force: true})
    cy.url().should('eq', Cypress.config().baseUrl + '/editTemplate') // => true
  })

  it('changes route to team-creation when "Build Team" is clicked', () => {
    cy.visit("/")
    cy.contains("Build Team").click({force: true})
    cy.url().should('eq', Cypress.config().baseUrl + '/team-creation') // => true
  })

  it('changes route to team-edition when "Edit Team" is clicked', () => {
    cy.visit("/")
    cy.contains("Edit Team").click({force: true})
    cy.url().should('eq', Cypress.config().baseUrl + '/team-edition') // => true
  })
})
