describe('Testing room-creation page', () => {
  it('Visits the room-creation page', () => {
    cy.visit("/room-creation")
  })

  it('displays GUI when "Build" is clicked', () => {
    cy.get("#build").click()
    cy.get("table")
  })

  it('creates the GUI with the row and col size entered', () => {
    cy.reload()
    cy.get("#roomID").clear().type("cypress")
    cy.get("#rowNum").clear().type("15")
    cy.get("#colNum").clear().type("15")
    cy.get("#build").click()
    cy.get("table").find("td").should("have.length", 225)
  })

  it('highlights the seats when dragged',() => {
    cy.contains("A,1").trigger("mousedown", {which:1})
    cy.contains("A,3").trigger("mousemove").trigger("mouseup")
    cy.contains("A,1").should("have.class", "highlight")
    cy.contains("A,2").should("have.class", "highlight")
    cy.contains("A,3").should("have.class", "highlight")
    cy.contains("A,1").trigger("mousedown", {which:1})
    cy.contains("A,3").trigger("mousemove").trigger("mouseup")
    cy.contains("A,1").should("not.have.class", "highlight")
    cy.contains("A,2").should("not.have.class", "highlight")
    cy.contains("A,3").should("not.have.class", "highlight")
  })

  it('assigns highlighted seats corresponding class names when different label buttons are clicked', () => {
    cy.contains("B,1").click()
    cy.get("#leftHandedButton").click()
    cy.contains("B,1").should("have.class","leftHand")

    cy.contains("B,1").click()
    cy.get("#aisleButton").click()
    cy.contains("B,1").should("have.class","aisleLeft")
    cy.contains("B,2").should("have.class","aisleRight")

    cy.contains("B,1").click()
    cy.get("#frontRowButton").click()
    cy.contains("B,1").should("have.class","front")

    cy.contains("B,1").click()
    cy.get("#backRowButton").click()
    cy.contains("B,1").should("have.class","back")

    cy.contains("B,1").click()
    cy.get("#frontishButton").click()
    cy.contains("B,1").should("have.class","frontish")

    cy.contains("B,1").click()
    cy.get("#backishButton").click()
    cy.contains("B,1").should("have.class","backish")

    cy.contains("B,1").click()
    cy.get("#leftButton").click()
    cy.contains("B,1").should("have.class","left")

    cy.contains("B,1").click()
    cy.get("#middleButton").click()
    cy.contains("B,1").should("have.class","middle")

    cy.contains("B,1").click()
    cy.get("#rightButton").click()
    cy.contains("B,1").should("have.class","right")

    cy.contains("B,1").click()
    cy.get("#brokenButton").click()
    cy.contains("B,1").should("have.class","broken")
  })

  it('clears class names for all seats after "Reset Board" is clicked', () => {
    cy.get("#reset").click()
    cy.contains("B,1").should("not.have.class","front")
    cy.contains("B,1").should("not.have.class","frontish")
    cy.contains("B,1").should("not.have.class","back")
    cy.contains("B,1").should("not.have.class","backish")
    cy.contains("B,1").should("not.have.class","left")
    cy.contains("B,1").should("not.have.class","middle")
    cy.contains("B,1").should("not.have.class","right")
    cy.contains("B,1").should("not.have.class","leftHanded")
    cy.contains("B,1").should("not.have.class","broken")
    cy.contains("B,1").should("not.have.class","aisleLeft")
    cy.contains("B,1").should("not.have.class","aisleRight")

    cy.contains("A,0").should("not.have.class","front")
    cy.contains("A,1").should("not.have.class","front")
    cy.contains("A,2").should("not.have.class","front")
    cy.contains("A,3").should("not.have.class","front")
    cy.contains("A,4").should("not.have.class","front")
    cy.contains("A,5").should("not.have.class","front")
    cy.contains("A,6").should("not.have.class","front")
    cy.contains("A,7").should("not.have.class","front")
    cy.contains("A,8").should("not.have.class","front")
    cy.contains("A,9").should("not.have.class","front")

    cy.contains("J,0").should("not.have.class","back")
    cy.contains("J,1").should("not.have.class","back")
    cy.contains("J,2").should("not.have.class","back")
    cy.contains("J,3").should("not.have.class","back")
    cy.contains("J,4").should("not.have.class","back")
    cy.contains("J,5").should("not.have.class","back")
    cy.contains("J,6").should("not.have.class","back")
    cy.contains("J,7").should("not.have.class","back")
    cy.contains("J,8").should("not.have.class","back")
    cy.contains("J,9").should("not.have.class","back")

  })

  it('sends post request to /room-saver on the server, and redirects to team-creation route', () => {
    cy.get("#saveChanges").click()
    cy.url().should("eq",Cypress.config().baseUrl + "/team-creation")
  })
})
