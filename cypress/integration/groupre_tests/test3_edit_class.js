describe('Testing editTemplate page', () => {
  it('visits the editTemplate page', () => {
    cy.visit("/editTemplate")
  })

  it('redirects to URL room-creation when "make new template" is clicked',() => {
    cy.get("#newClass").click()
    cy.url().should("eq",Cypress.config().baseUrl + "/room-creation")
  })

  it('redirects to URL with selected template when "go" is clicked',() => {
    cy.visit("/editTemplate")
    // need to add dropdown chec  k
    // selecting cypress room from dropdown
    cy.get('select[id="classList"]').select('cypress')
    cy.get('button[id="goToClass"]').click()
  })

  it('displays GUI when "Build" is clicked', () => {
    cy.get("#build-e").click()
    cy.get("table")
  })

  it('creates the GUI with the row and col size from selected template', () => {
    cy.get("table").find("td").should("have.length", 225)
  })

  it('highlights the seats when dragged',() => {
    cy.contains("A,1").trigger("mousedown", {which:1})
    cy.contains("A,3").trigger("mousemove").trigger("mouseup")
    cy.contains("A,1").should("have.class", "highlight")
    cy.contains("A,2").should("have.class", "highlight")
    cy.contains("A,3").should("have.class", "highlight")
    cy.contains("A,1").trigger("mousedown", {which:1})
    cy.contains("A,3").trigger("mousemove").trigger("mouseup")
    cy.contains("A,1").should("not.have.class", "highlight")
    cy.contains("A,2").should("not.have.class", "highlight")
    cy.contains("A,3").should("not.have.class", "highlight")
  })

  it("should contain seats with no attributes because it was saved from reset", () => {
    cy.contains("A,0").should("not.have.class","front")
    cy.contains("A,1").should("not.have.class","front")
    cy.contains("A,2").should("not.have.class","front")
    cy.contains("A,3").should("not.have.class","front")
    cy.contains("A,4").should("not.have.class","front")
    cy.contains("A,5").should("not.have.class","front")
    cy.contains("A,6").should("not.have.class","front")
    cy.contains("A,7").should("not.have.class","front")
    cy.contains("A,8").should("not.have.class","front")
    cy.contains("A,9").should("not.have.class","front")
    cy.contains("A,10").should("not.have.class","front")
    cy.contains("A,11").should("not.have.class","front")
    cy.contains("A,12").should("not.have.class","front")
    cy.contains("A,13").should("not.have.class","front")
    cy.contains("A,14").should("not.have.class","front")

    cy.contains("J,0").should("not.have.class","back")
    cy.contains("J,1").should("not.have.class","back")
    cy.contains("J,2").should("not.have.class","back")
    cy.contains("J,3").should("not.have.class","back")
    cy.contains("J,4").should("not.have.class","back")
    cy.contains("J,5").should("not.have.class","back")
    cy.contains("J,6").should("not.have.class","back")
    cy.contains("J,7").should("not.have.class","back")
    cy.contains("J,8").should("not.have.class","back")
    cy.contains("J,9").should("not.have.class","back")
    cy.contains("J,10").should("not.have.class","back")
    cy.contains("J,11").should("not.have.class","back")
    cy.contains("J,12").should("not.have.class","back")
    cy.contains("J,13").should("not.have.class","back")
    cy.contains("J,14").should("not.have.class","back")
  })

  it('assigns highlighted seats corresponding class names when different label buttons are clicked', () => {
    cy.contains("B,1").click()
    cy.get("#leftHandedButton-e").click()
    cy.contains("B,1").should("have.class","leftHand")

    cy.contains("B,1").click()
    cy.get("#aisleButton-e").click()
    cy.contains("B,1").should("have.class","aisleLeft")
    cy.contains("B,2").should("have.class","aisleRight")

    cy.contains("B,1").click()
    cy.get("#frontRowButton-e").click()
    cy.contains("B,1").should("have.class","front")

    cy.contains("B,1").click()
    cy.get("#backRowButton-e").click()
    cy.contains("B,1").should("have.class","back")

    cy.contains("B,1").click()
    cy.get("#frontishButton-e").click()
    cy.contains("B,1").should("have.class","frontish")

    cy.contains("B,1").click()
    cy.get("#backishButton-e").click()
    cy.contains("B,1").should("have.class","backish")

    cy.contains("B,1").click()
    cy.get("#leftButton-e").click()
    cy.contains("B,1").should("have.class","left")

    cy.contains("B,1").click()
    cy.get("#middleButton-e").click()
    cy.contains("B,1").should("have.class","middle")

    cy.contains("B,1").click()
    cy.get("#rightButton-e").click()
    cy.contains("B,1").should("have.class","right")

    cy.contains("B,1").click()
    cy.get("#brokenButton-e").click()
    cy.contains("B,1").should("have.class","broken")
  })

  it('clears class names for all seats after "Reset Board" is clicked', () => {
    cy.get("#reset-e").click()
    cy.contains("B,1").should("not.have.class","front")
    cy.contains("B,1").should("not.have.class","frontish")
    cy.contains("B,1").should("not.have.class","back")
    cy.contains("B,1").should("not.have.class","backish")
    cy.contains("B,1").should("not.have.class","left")
    cy.contains("B,1").should("not.have.class","middle")
    cy.contains("B,1").should("not.have.class","right")
    cy.contains("B,1").should("not.have.class","leftHanded")
    cy.contains("B,1").should("not.have.class","broken")
    cy.contains("B,1").should("not.have.class","aisleLeft")
    cy.contains("B,1").should("not.have.class","aisleRight")

    cy.contains("A,0").should("not.have.class","front")
    cy.contains("A,1").should("not.have.class","front")
    cy.contains("A,2").should("not.have.class","front")
    cy.contains("A,3").should("not.have.class","front")
    cy.contains("A,4").should("not.have.class","front")
    cy.contains("A,5").should("not.have.class","front")
    cy.contains("A,6").should("not.have.class","front")
    cy.contains("A,7").should("not.have.class","front")
    cy.contains("A,8").should("not.have.class","front")
    cy.contains("A,9").should("not.have.class","front")

    cy.contains("J,0").should("not.have.class","back")
    cy.contains("J,1").should("not.have.class","back")
    cy.contains("J,2").should("not.have.class","back")
    cy.contains("J,3").should("not.have.class","back")
    cy.contains("J,4").should("not.have.class","back")
    cy.contains("J,5").should("not.have.class","back")
    cy.contains("J,6").should("not.have.class","back")
    cy.contains("J,7").should("not.have.class","back")
    cy.contains("J,8").should("not.have.class","back")
    cy.contains("J,9").should("not.have.class","back")
  })

  it('sends post request to /room-saver on the server, and redirects to team-creation route', () => {
    cy.get("#saveChanges-e").click()
    cy.url().should("eq",Cypress.config().baseUrl + "/team-creation")
  })
})
