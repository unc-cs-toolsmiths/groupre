describe('Testing team-creation page', () => {
  it('visits the team-creation page', () => {
    cy.visit("/team-creation")
  })

  it('has a dropdown menu containing existing teamplates', () => {
    cy.get('select').select('cypress')
  })

  it('redirects to URL containing the template information when "go" is clicked', () => {
    cy.contains("Go").click()
    cy.url().should("eq",Cypress.config().baseUrl + "/template/template-cypress-RowCol-15-15.json")
  })

  it('displays GUI when "Build" is clicked', () => {
    cy.get("#buildClass").click()
    cy.get("table")
  })

  it('creates the GUI with the row and col size from the selected teamplate', () => {
    cy.reload()
    cy.get("#teamName").clear().type("team1")
    cy.get("#buildClass").click()
    cy.get("table").find("td").should("have.length", 225)
  })

  it('highlights the seats when dragged',() => {
    cy.contains("A,1").trigger("mousedown", {which:1})
    cy.contains("A,3").trigger("mousemove").trigger("mouseup")
    cy.contains("A,1").should("have.class", "highlight")
    cy.contains("A,2").should("have.class", "highlight")
    cy.contains("A,3").should("have.class", "highlight")
    cy.contains("A,1").trigger("mousedown", {which:1})
    cy.contains("A,3").trigger("mousemove").trigger("mouseup")
    cy.contains("A,1").should("not.have.class", "highlight")
    cy.contains("A,2").should("not.have.class", "highlight")
    cy.contains("A,3").should("not.have.class", "highlight")
  })

  it("should contain seats with no attributes because it was saved from reset", () => {
    cy.contains("A,0").should("not.have.class","front")
    cy.contains("A,1").should("not.have.class","front")
    cy.contains("A,2").should("not.have.class","front")
    cy.contains("A,3").should("not.have.class","front")
    cy.contains("A,4").should("not.have.class","front")
    cy.contains("A,5").should("not.have.class","front")
    cy.contains("A,6").should("not.have.class","front")
    cy.contains("A,7").should("not.have.class","front")
    cy.contains("A,8").should("not.have.class","front")
    cy.contains("A,9").should("not.have.class","front")
    cy.contains("A,10").should("not.have.class","front")
    cy.contains("A,11").should("not.have.class","front")
    cy.contains("A,12").should("not.have.class","front")
    cy.contains("A,13").should("not.have.class","front")
    cy.contains("A,14").should("not.have.class","front")

    cy.contains("J,0").should("not.have.class","back")
    cy.contains("J,1").should("not.have.class","back")
    cy.contains("J,2").should("not.have.class","back")
    cy.contains("J,3").should("not.have.class","back")
    cy.contains("J,4").should("not.have.class","back")
    cy.contains("J,5").should("not.have.class","back")
    cy.contains("J,6").should("not.have.class","back")
    cy.contains("J,7").should("not.have.class","back")
    cy.contains("J,8").should("not.have.class","back")
    cy.contains("J,9").should("not.have.class","back")
    cy.contains("J,10").should("not.have.class","back")
    cy.contains("J,11").should("not.have.class","back")
    cy.contains("J,12").should("not.have.class","back")
    cy.contains("J,13").should("not.have.class","back")
    cy.contains("J,14").should("not.have.class","back")
  })

  it('assigns highlighted seats corresponding class names when different label buttons are clicked', () => {
    cy.contains("A,0").click()
    cy.get("#teamButton").click()
    cy.contains("A,1").click()
    cy.get("#teamButton").click()
    cy.contains("A,2").click()
    cy.get("#teamButton").click()
    cy.contains("A,3").click()
    cy.get("#teamButton").click()
    // cypress does get selector with a comma, so have to use contain
    cy.get("table").contains("0")
    cy.get("table").contains("1")
    cy.get("table").contains("2")
    cy.get("table").contains("3")

    cy.get("table").contains("0").click()
    cy.get("#removeTeam").click()
    cy.get("table").contains("1").click()
    cy.get("#removeTeam").click()
    cy.get("table").contains("2").click()
    cy.get("#removeTeam").click()
    cy.get("table").contains("3").click()
    cy.get("#removeTeam").click()
    cy.get("table").contains("A,0")
    cy.get("table").contains("A,1")
    cy.get("table").contains("A,2")
    cy.get("table").contains("A,3")

    // need a way to check autogrouping button

  })

  it('clears class names for all seats after "Reset Board" is clicked', () => {
    cy.get("#resetTeam").click();
    cy.get("table").contains("A,0")
    cy.get("table").contains("A,1")
    cy.get("table").contains("A,2")
    cy.get("table").contains("A,3")
  })

  it('sends post request to /room-saver on the server, and downloads file', () => {
    cy.get("#saveTeam").click()
    // const exist_cmd = Cypress.platform === 'win32' ? "if exist ..\\..\\Downloads\\room-cypress-team1-15-15.csv echo It exists!" : "[ -f ~/Downloads/output.csv ] && echo 'It exists!'"
    // cy.exec(exist_cmd).its('stdout').should('contain', 'It exists')
    // const delete_cmd = Cypress.platform === "win32" ? "del ..\\..\\Downloads\\room-cypress-team1-15-15.csv" : "rm ~/Downloads/room-cypress-team1-15-15.csv"
    // cy.exec(delete_cmd)
  })
})
