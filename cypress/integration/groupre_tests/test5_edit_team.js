describe('Testing team-edition page', () => {
  it('visits the team-edition page', () => {
    cy.visit("/team-edition")
  })

  it('has a dropdown menu containing existing chair.csvs', () => {
    cy.get('select').select('room1')
  })

  it('redirects to URL containing the csv information when "go" is clicked', () => {
    cy.get("#goEditClass").click()
    cy.url().should("eq", Cypress.config().baseUrl + "/chair/room-test-room1-10-10.csv")
  })

  it('displays GUI when "Build" is clicked', () => {
    cy.get("#buildClass-e").click()
    cy.get("table")
  })

  it('creates the GUI with the row and col size of previous chair.csv is selected', () => {
    cy.get("table").find("tr").should("have.length", 10)
    cy.get("table").find("tr").eq(9).find("td").should("have.length", 10)
  })

  it('highlights the seats when dragged', () => {
    cy.get(".front").eq(1)
      .trigger("mousedown", { which: 1 })
    cy.get(".front").eq(4)
      .trigger("mousemove")
      .trigger("mouseup")
  })

  it('assigns highlighted seats corresponding class names', () => {
    cy.get(".front").eq(1)
      .should("have.class", "highlight")
    cy.get(".front").eq(2)
      .should("have.class", "highlight")
    cy.get(".front").eq(3)
      .should("have.class", "highlight")
    cy.get(".front").eq(4)
      .should("have.class", "highlight")
  })

  it('clears class names for all seats after "build team" of 2 and then "Reset Board" is clicked', () => {
    cy.get("#autoAdd-e").click()
    cy.get(".front").eq(5)
      .should("have.class", "front team2")
    cy.get("#resetTeam-e").click()
    cy.get(".front").eq(5)
      .should("have.class", "front")
  })

  it('sends post request to /class-saver on the server, and renders the download page', () => {
    cy.get("#saveTeam-e").click()
    cy.url().should("eq",Cypress.config().baseUrl + "/chair/room-test-room1-10-10.csv")
  })
})
