import os
import json
from flask import session
#import pyrebase
import firebase_admin
from firebase_admin import _user_mgt
from firebase_admin.exceptions import FirebaseError

## Define directory
temp_file = os.getcwd()
firebase_key = json.loads(os.environ['FIREBASE_KEY']) if 'FIREBASE_KEY' in os.environ else os.path.normpath(temp_file+'/key.json')
firebase_fbconfig = json.loads(os.environ['FIREBASE_CONFIG']) if 'FIREBASE_CONFIG' in os.environ else json.load(open(os.path.normpath(temp_file+'/fbconfig.json')))



#pb = pyrebase.initialize_app(firebase_fbconfig) || Pyrebase is outdated
creds = firebase_admin.credentials.Certificate(firebase_key)
fb = firebase_admin.initialize_app(creds)

from firebase_admin import auth as au
from firebase_admin import db as d
auth = au.Client(fb)
db = d
# 'auth' This variable will allow you to register/login users
# 'db' This variable allows you to retrieve/set data from/for the database.