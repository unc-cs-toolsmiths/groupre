import json
import os
from werkzeug.utils import secure_filename
from werkzeug.wrappers import Response
from flask import (flash, redirect, render_template, request,
                   url_for, current_app, jsonify)
import csv
'''
Home dir
    displays all the buttons for navigation
'''
def docs():
    path = os.path.dirname(os.path.realpath(__file__)) + '/static/docs/archive'
    return render_template('dirtree.html', tree=make_tree(path))
def run_groupre_program():
    msg = ''
    if request.method == 'POST':
        if 'pref.csv' not in request.files:
            #  flash('No file part')
            flash('preference_file doesn\'t exist')
        if 'chair.csv' not in request.files:
            #  flash('No file part')
            flash('chair_file doesn\'t exist')
        if 'roster.csv' not in request.files:
            #  flash('No file part')
            flash('roster_file doesn\'t exist')

        preference_file = request.files['pref.csv']
        chair_file = request.files['chair.csv']
        roster_file = request.files['roster.csv']

        # if user does not select file, browser also
        # submit a empty part without filename
        if preference_file.filename == '':
            flash('No selected preference file')
        if chair_file.filename == '':
            flash('No selected chair file')
        if roster_file.filename == '':
            flash('No selected roster file')
        if preference_file and chair_file and roster_file:
            preference_filename = secure_filename(preference_file.filename)
            chair_filename = secure_filename(chair_file.filename)
            roster_filename = secure_filename(roster_file.filename)

            preference_location = os.path.join(current_app.config['UPLOAD_FOLDER'], preference_filename)
            chair_location = os.path.join(current_app.config['UPLOAD_FOLDER'], chair_filename)
            roster_location = os.path.join(current_app.config['UPLOAD_FOLDER'], roster_filename)

            preference_file.save(preference_location)
            chair_file.save(chair_location)
            roster_file.save(roster_location)

            result = run_new_groupre(preference_location, chair_location, roster_location)
            os.remove(preference_location)
            os.remove(chair_location)
            os.remove(roster_location)
            output_name = result[0]
            msg = result[1]
            output_name = output_name.split(os.path.normpath('/'))[-1].split('.', 1)[0]
            output_names = output_name + '.csv'

            files = downloadcsv(output_names)
            return files
            # return send_file(files.data, mimetype="text/csv", attachment_filename="output.csv", as_attachment=True)
            # redirect(url_for(f'downloadcsv', output_name=output_names), code=307)
            # return render_template("metrics.html", output_name=output_name, title = "Metrics result",msg = msg)
    return render_template('run_groupre.html', title="Run Groupre Program")


def results(output_name,msg):
    return render_template("metrics.html",output_name=output_name,msg=msg)

def upload_file(roomID):
    # This is options for grouping
    fallback = False
    test = False
    if request.method == 'POST':
        if (request.form.get('teamOpt')) == 'test':
            test = True
        if 'file' not in request.files:
            #  flash('No file part')
            flash('Your file doesnt exist')
            return redirect(url_for('selectRoom'))
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(url_for('selectRoom'))
        if file and allowed_file(file.filename):

            filename = secure_filename(file.filename)
            newlocation = os.path.join(current_app.config['UPLOAD_FOLDER'], filename)
            file.save(newlocation)
            capacity = int(roomID.split('-')[-2]) * int(roomID.split('-')[-1])
            with open(newlocation, newline='') as csvfile:
                reader = csv.reader(csvfile, delimiter=',')
                row_count = sum(1 for row in reader) - 1
            if row_count > capacity:
                flash('Students more than num of seats')
                return redirect(url_for('selectRoom'))

            roomID = current_app.config['CHAIRS_DIR'] + roomID + '.csv'
            if test:
                output_name = run_groupre_test(newlocation, roomID)
            else:
                output_name = run_groupre(newlocation, roomID)

            output_name = output_name.split('/')[-1].split('.', 1)[0]
            return redirect('/metrics/' + output_name)
    return render_template('upload.html', title="Loaded " + str(roomID))


def downloadChair(output_name):
    return render_template("metrics.html", output_name=output_name, title = "download chair.csv",msg = "Ready for download")

'''
Room selection page for running the groupre program with a student.csv
'''
def selectRoom():
    chairFiles = {}
    for cFile in os.listdir(current_app.config['CHAIRS_DIR']):
        if '.csv' in cFile:
            cValue = cFile.split('.csv')[0]
            cKey = cValue.split('-')
            cKey = cKey[2:]
            roomID = cKey[0].title()
            capacity = ' ' + str(int(cKey[1]) * int(cKey[2])) + ' Students'
            cKey = roomID + capacity
            chairFiles.update({cKey:cValue})
    return render_template("room.html", chairFiles=chairFiles, title = "Run Groupre")
'''
Allows webapp to locate the output file and download it to user's computer
'''
def downloadcsv(output_name):
    if "room-" in output_name:
        with open(os.path.normpath(current_app.config['CHAIRS_DIR'] + "/" + output_name), 'r') as file:
            reader = csv.reader(file, delimiter=',')
            csvfile = []
            for row in reader:
                for field in row:
                    csvfile += str(field) + ','
                csvfile += '\n'
        return Response(
            csvfile,
            mimetype="text/csv",
            headers={"Content-disposition":
                         "attachment; filename=" + output_name})

    with open(os.path.normpath(current_app.config['UPLOAD_FOLDER'] + "/output/" + output_name), 'r') as file:
        reader = csv.reader(file, delimiter=',')
        csvfile = []
        print("reading")
        for row in reader:
            for field in row:
                csvfile += str(field) + ','
            csvfile += '\n'
    os.remove(os.path.normpath(current_app.config['UPLOAD_FOLDER'] + "/output/" + output_name))
    return Response(
        csvfile,
        mimetype="text/csv",
        headers={"Content-disposition":
                     "attachment; filename=output.csv"})


'''
Retrieves room template back to team-creation for creating teams
'''
def retrieve_file(jsonName):
    # returns json files to javascript
    filepath =os.path.normpath(current_app.config['CLASSROOMS_DIR'] + "/" + jsonName)
    with open(filepath, 'r') as f:
        jdata = json.load(f)
    return render_template('groupreTeam.html', jdata = jdata , name = jsonName, title = "Create Team")

'''
Retrieves final room template back to team-edition for editing teams
'''
def retrieve_team(jsonName):
    # returns json files to javascript
    jdata = []
    filepath =os.path.normpath(current_app.config['CHAIRS_DIR'] + "/" + jsonName)
    print(filepath)
    with open(filepath, 'r') as f:
        csvReader = csv.reader(f)
        for row in csvReader:
            jdata.append(row)
        # jdata = json.load(jdata)
    return render_template('editTeam.html', jdata = jdata , name = jsonName, title = "Edit Team")

'''
Retrieves classroom template back to room-creation for editing rooms
'''
def retrieve_class(jsonName):
    # returns json files to javascript
    filepath =os.path.normpath(current_app.config['CLASSROOMS_DIR'] + "/" + jsonName)
    with open(filepath, 'r') as f:
        jdata = json.load(f)
    return render_template('editClass.html', jdata = jdata , name = jsonName, title = "Edit Template")

'''
room creation dir
    An interface for create a customized classroom. 
    It allows the user to create template of classes
'''
def create_room():
    return render_template('groupreHome.html', title = "Create room")

'''
chooses the existing classroom template for edit
'''
# directs to a page that allows user to decide wheather to change template or make new
def changeTemplate():
    roomFiles = {}
    for rFile in os.listdir(current_app.config['CLASSROOMS_DIR']):
        if '.json' in rFile:
            rKey = rFile.split('-')[1]
            roomFiles[rKey]= rFile

    return render_template('chooseClass.html', roomFiles = roomFiles, title = "Edit Room")

'''
Team creation dir
    An interface for create group of seat in a classroom. 
    It allows the user to create template of teams in a classroom.json
'''
def create_team():
    roomFiles = {}
    for rFile in os.listdir(current_app.config['CLASSROOMS_DIR']):
        if '.json' in rFile:
            rKey = rFile.split('-')[1]
            roomFiles[rKey]= rFile
    return render_template('chooseTeam.html', roomFiles = roomFiles , title = "Create teams")

'''
Edit team dir
    An interface same as the team creation but it allows user to edit chairs.csv
'''
def edit_team():
    roomFiles = {}
    for rFile in os.listdir(current_app.config['CHAIRS_DIR']):
        if '.csv' in rFile:
            rKey = rFile.split('-')[2]
            roomFiles[rKey]= rFile
    return render_template('chooseEditTeams.html', roomFiles = roomFiles , title = "Edit teams")

'''
Saves final room with group information in /uploads/chairs
'''
def saveRoom():
    if request.method == 'POST':
        content = request.get_json()
        info = content.pop(0)
        filename = []
        for item in info:
            filename.append(str(item))
        filename = 'room-' + ('-'.join(filename))
        filepath = os.path.normpath(current_app.config['CHAIRS_DIR'] + "/" + filename + '.csv')
        with open(filepath, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for row in content:
                writer.writerow(row)
        files = downloadcsv(filename + ".csv")
        return files
    return redirect('/team-creation')

'''
Saves class templates in /uploads/classrooms
'''
def saveClass():
    content = request.get_json()
    info = content.pop(0)
    filename = []
    for item in info:
        filename.append(str(item))
    filename = '-'.join(filename)
    filename = os.path.normpath(current_app.config['CLASSROOMS_DIR'] + "/" + 'template-' + filename + '.json')
    with open(filename, 'w') as outfile:
        json.dump(content[1:],outfile,ensure_ascii=False)

    resp = jsonify(success=True)
    resp.status_code = 200
    return resp

from random import randint
from subprocess import Popen, PIPE
def make_tree(path):
    tree = dict(name=path.split('/')[-2], children=[])
    try:
        lst = os.listdir(path)
    except OSError:
        pass  # ignore errors
    else:
        for name in lst:
            fn = os.path.join(path, name)
            if os.path.isdir(fn):
                tree['children'].append(make_tree(fn))
            else:
                tree['children'].append(dict(name=name))
    return tree
def run_groupre_test (students, chairs):
    output_name = os.path.normpath(current_app.config['UPLOAD_FOLDER'] + '/output/' + \
    str(randint(1000000, 9999999)) + '' + \
    str(randint(1000000, 9999999)) + '.csv')
    arguments = 'python3.7 ./src/test.py -c {} -s {} -o {}'.format(chairs,students,output_name)
    os.system(arguments)
    return output_name

def run_new_groupre(preference, chair, roster):
    output_name = os.path.normpath(current_app.config['UPLOAD_FOLDER'] + '/output/' + \
    str(randint(1000000, 9999999)) + '' + \
    str(randint(1000000, 9999999)) + '.csv')
    temp = current_app.config['UPLOAD_FOLDER'][0:current_app.config['UPLOAD_FOLDER'].index('uploads')]
    p = Popen(['python', os.path.normpath(temp+'/src/groupre.py'),'-c',chair,'-s',preference,'-r',roster,'-o',output_name], stdout=PIPE, stderr=PIPE)
    # msg = p.stderr.read().decode("utf-8") + p.stdout.read().decode("utf-8")
    print(p.stdout.read().decode("utf-8"))
    msg = p.stderr.read().decode("utf-8").strip()

    print(msg)
    msg_list = msg.split("\n")
    return [output_name,msg_list]
def allowed_file(filename):
    return '.' in filename and filename.split('.', 1)[1].lower() in {'csv'}