from groupre_core_pages import flash, render_template, request, redirect
from database import auth, db, session, _user_mgt, FirebaseError
from firebase_admin.auth import UserNotFoundError
import json
import re

regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
def sign_up():
    if 'uid' in session and session['uid'] is not None:
        return redirect('/account')
    if request.method == 'POST':
        if len(list(request.form.keys()))!=3 and 'email' not in request.form.keys() and 'password' not in request.form.keys() and 'confirm_password' not in request.form.keys():
            flash('Please only send valid form data')
            return render_template("signUp.html")

        #add sign up stuff with session here later
        sign_up_email = request.form['email']
        sign_up_password = request.form['password']
        sign_up_confirm_pass = request.form['confirm_password']

        #Validating the data on the form if someone bypassed the client-side validation:
        #The regex code came from https://www.geeksforgeeks.org/check-if-email-address-valid-or-not-in-python/
        if not(re.search(regex,sign_up_email)):
            flash('Please enter a valid email')
            return render_template("signUp.html")
        if len(sign_up_password)<6:
            flash('Please make your password have at least 6 characters')
            return render_template("signUp.html")
        if sign_up_password != sign_up_confirm_pass:
            flash('Please make sure both of the passwords you entered match each other')
            return render_template("signUp.html")

        try:
            #Returns UserRecord type from _user_mgt
            auth.get_user_by_email(email=sign_up_email)
            flash('An account with this email already exists, please go to the login page instead')
            return render_template("signUp.html")
        except UserNotFoundError:
            try:
                user : _user_mgt.UserRecord = auth.create_user(email=sign_up_email, password=sign_up_password, email_verified=False)
                session['uid'] = user.uid
                return redirect('/account')
            except ValueError:
                flash('Make sure all of the data you provided is correct')
                return render_template("signUp.html")
            except FirebaseError:
                flash('An error occurred while trying to create your account; please try again later')
                return render_template("signUp.html")
    return render_template("signUp.html")


def login():
    if request.method == 'POST':
        if len(request.form.keys())!=2 and 'email' not in request.form.keys() and 'password' not in request.form.keys():
            flash('Please only send valid form data')
            return render_template("login.html")

        # add login stuff with session here later
        login_email = request.form['email']
        login_password = request.form['password']

        # Validating the data on the form if someone bypassed the client-side validation:
        # The regex code came from https://www.geeksforgeeks.org/check-if-email-address-valid-or-not-in-python/
        if not(re.search(regex,login_email)):
            flash('Please enter a valid email')
            return render_template("login.html")
        if len(login_password)<6:
            flash('Please enter a valid password')
            return render_template("login.html")

        try:
            #Returns UserRecord type from _user_mgt
            auth
            flash('An account with this email already exists, please go to the login page instead')
            return redirect('/account')
        except auth.UserNotFoundError:
            flash('No account found with that email/password')
            return render_template('login.html')

    return render_template("login.html")

def student_form():
    return render_template("student_survey.html")

def account_page():
    if 'uid' in session and session['uid'] is not None:
        return render_template('account.html')
    else:
        return redirect('/sign-up')
