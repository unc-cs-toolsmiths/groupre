from data_structures.student import Student
from data_structures.chair import Chair

class Output:
	
	chair: Chair = None
	student: Student = None
	group_id: int = None

	def __init__(self, chair, student, group_id):
		self.chair = chair
		self. student = student
		self.group_id = group_id
		
	def __str__(self):
		return ("Chair: " + self.chair.chair_id + " Student: " + self.student.onyen+ " Group ID: " + self.group_id)
	
