class Validator:
    def __init__(self,file_type):
        self.file_type = file_type
    def check_header(self,input_file):
        if self.file_type == "student":
            onyen = input_file[3].strip().lower()
            pid = input_file[4].strip().lower()
            frontback = input_file[5].strip().lower()
            section =  input_file[6].strip().lower()
            left_handed = input_file[7].strip().lower()
            aisle = input_file[8].strip().lower()
            if "What is your UNC onyen?".strip().lower() !=  onyen:
                raise NameError("'What is your UNC onyen?' not in header instead got " + onyen)
            if "Enter your UNC PID (enter a 7 digit number)".strip().lower() != pid:
                raise NameError("'Enter your UNC PID (enter a 7 digit number)' not in header instead got " + pid)
            if "Which row is important to you? If we are unable to accommodate a specific request, we will place you as close to your requested location as possible.".strip().lower() != frontback:
                raise NameError("'Which row is important to you? If we are unable to accommodate a specific request, we will place you as close to your requested location as possible.' is not in header instead got " + frontback)
            if "When facing the screens, what region of the classroom is important to you?".strip().lower() != section:
                raise NameError("'When facing the screens, what region of the classroom is important to you?' is not in header instead got " +  input_file[6].strip().lower())
            if "Do you require a left handed desk?".strip().lower() != left_handed:
                raise NameError("'Do you require a left handed desk?' is not in header instead got " + left_handed)
            if "Do you need to sit in an aisle seat?".strip().lower() != aisle:
                raise NameError("'Do you need to sit in an aisle seat?' not in header instead got " + aisle)
        if self.file_type == "chair":
            # stripping BOM (byte order mark)
            cid = input_file[0].strip("ï»¿").strip().lower()
            group_id = input_file[1].strip().lower()
            preferences = input_file[2].strip().lower()
            if cid != "CID".strip().lower():
                raise NameError("CID is not in the header and got " + cid)
            if group_id != "TeamID".strip().lower():
                raise NameError("TeamID is not in the header and got " + group_id)
            if preferences != "Preferences".strip().lower():
                raise NameError("Preferences is not in the header and got " + preferences)
        if self.file_type == "roster":
            # Input rows look like:
            # PID, "lastname, firstname", onyen@live.unc.edu
            pid = input_file[0].strip("ï»¿").strip().lower()
            name = input_file[1].strip().lower()
            email = input_file[2].strip().lower()
            if pid != "ID".strip().lower():
                raise NameError("ID is not the first field in the header instead we got " + pid)
            if name != "Name".strip().lower():
                raise NameError("Name is not the second field in the header instead we got " + name)
            if email != "Email".strip().lower():
                raise NameError("Email is not the third field in the header instead we got " + email)
        return True
    def check_consent(self,input_file):
        try:
            if input_file[3] != "0" and input_file[3] != "1":
                raise ValueError("the Status Note in roster should be 0 or 1, can't not be empty or others")
        except IndexError:
            raise ValueError("the Status Note in roster should be 0 or 1, can't not be empty or others")
    def check_email(self,input_file):
        if "@" not in input_file[2]:
            raise ValueError("no email format was found @ symbol not in email")
    def check_PID(self,input_file):
        if self.file_type == "student":
            if len(input_file[4].strip()) != 9:
                raise ValueError(input_file[4] + " is not 9 digits")
        if self.file_type == "roster":
            if len(input_file[0].strip()) != 9:
                raise ValueError(input_file[0] + " is not 9 digits")
        return True
    def check_VIP(self,input_file):
        vip = ""
        if len(input_file)>=10:
            try:
                vip = input_file[10].strip().lower()
            except IndexError:
                vip = ""
        if vip != "yes" and vip != "no" and len(vip) != 0:
            raise ValueError(vip+" is not a valid VIP type")
        return True
    def check_chair_preferences(self,input_file):
        for x in input_file[2:]:
            if x not in ["f","fi","b","bi","la","a", "left","middle","right", ""]:
                raise ValueError(x+" is not a valid preferences type")
        return True
    def check_student_preferences(self,input_file):
        if input_file[5] not in ["Front row","Anywhere in the first seven front rows","The last row possible (NOTE: this may be around row N, and not the last row of the classroom)","Towards the back of the classroom","No preference for location in the room",""]:
            raise ValueError(input_file[5] + " is not a valid preference type")
        if input_file[6] not in ["The large middle section", "The left hand section (when I'm facing the screens)","The right hand section (when I'm facing the screens)","No preference for region of the classroom",""]:
            raise ValueError(input_file[6]+" is not a valid preferences type")
        if input_file[7] not in ["Yes","No",""]:
                raise ValueError(input_file[7]+" is not a valid preferences type","")
        if input_file[8] not in ["Yes","No"]:
            raise ValueError(input_file[7]+" is not a valid preferences type","")
        return True
