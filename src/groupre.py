#!/usr/bin/env python
'''This module will be used to take input from a chairs.csv and a preference.csv and a class roster.csv
 and return a csv of sorted teams to their prefered seats.'''

import argparse
import csv
import sys
import time
from typing import List

from data_structures.student import Student
from data_structures.chair import Chair
from data_structures.validator import Validator
from seat_assignment_functions import *

def input_checking(chairs_csv,preference_csv,roster_csv,output_csv):
    if chairs_csv is None:
        print('Missing chairs input file.')
        return False
    if '.csv' not in chairs_csv:
        print('Chairs input is of wrong format. Try uploading a .csv instead.')
        return False

    if preference_csv is None:
        print('Missing students input file.')
        return False
    if '.csv' not in preference_csv:
        print('Students input is of wrong format. Try uploading a .csv instead.')
        return False

    if roster_csv is None:
        print('Missing roster input file.')
        return False
    if '.csv' not in roster_csv:
        print('Roster input is of wrong format. Try uploading a .csv instead')
        return False

    if output_csv is None:
        print('''Output file not specified, and the default was somehow
                replaced. Please try specifying a proper output file.''')
        return False
    if '.csv' not in output_csv:
        print('output file should have a .csv extension. Try again')
        return False
    return True
def get_raw(csv_file):
    lines = []
    with open(csv_file, 'r', encoding='cp1252') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            lines.append(row)
    return lines
def preference_validation(preferences):
    student_validator = Validator("student")
    first_line = True
    for row in preferences:
        row = [line.strip() for line in row]
        if first_line: 
            first_line = False
            student_validator.check_header(row)
            continue
        student_validator.check_PID(row)
        print(row)
        student_validator.check_VIP(row)
        student_validator.check_student_preferences(row)
def chair_validation(chairs):
    chair_validator = Validator("chair")
    first_line = True
    for row in chairs:
        row = [line.strip() for line in row]
        if first_line:
            first_line = False
            chair_validator.check_header(row)
        else:
            chair_validator.check_chair_preferences(row)
def roster_validation(rosters):
    roster_validator = Validator("roster")
    first_line = True
    second_line = True
    for row in rosters:
        if first_line:
            first_line = False
            continue
        if second_line:
            roster_validator.check_header(row)
            second_line = False
            continue
        roster_validator.check_PID(row)
        roster_validator.check_consent(row)
        roster_validator.check_email(row)

def read_preference_file(preferences):
    student_list = []
    student_tupel_list = set([])
    first_line = True
    for row in preferences:
        row = [line.strip() for line in row]
        if first_line: 
            first_line = False
            continue
        temp_student_tuple = tuple([row[3].lower(),str(row[4])])
        len_before = len(student_tupel_list)
        student_tupel_list.add(temp_student_tuple)
        len_after = len(student_tupel_list)
        # check for duplication
        if len_after > len_before:
            prefs = convert_pref(row[5],row[6],row[7],row[8])
            student_list.append(Student(row[3],row[4],row[10], prefs))
    return student_list, student_tupel_list
def read_chair_file(chairs):
    chair_list = []
    first_line = True
    for row in chairs:
        row = [line.strip() for line in row]
        if first_line:
            first_line = False
        else:
            chair_list.append(Chair(row[0], row[1], row[2:]))
    return chair_list
def read_roster_file(rosters):
    roster_list = set([])
    consent_map = {}
    roster_validator = Validator("roster")
    first_line = True
    second_line = True
    for row in rosters:
        if first_line:
            first_line = False
            continue
        if second_line:
            roster_validator.check_header(row)
            second_line = False
            continue
        roster_validator.check_PID(row)
        roster_validator.check_consent(row)
        # Extract the onyen from the email (row[2])
        x = row[2].split('@')[0]
        PID = row[0]
        consent_map[PID] = int(row[3])
        temp_roster_tuple = tuple([x.lower(), str(PID)])
        roster_list.add(temp_roster_tuple)
    return roster_list, consent_map
def write_output(output_csv,newPairs):
    # Write our output to a csv.
    # NOTE 'newline=''' required when writing on an OS that ends lines in CRLF rather than just LF.
    print('----------')
    print('Seats assigned. Writing to csv.')
    # writing outputs
    with open(output_csv,"w",newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=',')
        writer.writerow(["chair id","group id", "PID", "Onyen", "Is VIP", "Student Consent","Group consent","Student prefs", "Chair Prefs"])
        for x in newPairs:
            # writer.writerow([x[0].chair_id,x[0].group_id,x[1].student_id,x[1].onyen])
            writer.writerow([x[0].chair_id,x[0].group_id,x[1].student_id,x[1].onyen,x[1].is_VIP,x[1].consented,x[0].consented,x[1].prefs,x[0].prefs])

def not_in_roster_error(student_tupel_list,roster_list):
    students_not_in_roster = student_tupel_list.difference(roster_list)
    if len(students_not_in_roster) > 0:
        for s in students_not_in_roster:
            raise ValueError(s[0]+ "," + s[1] + " is not in the roster")
def metric(output_list):
    # See if the output has the students and chairs preferences matching
    preference_matches = []
    one_match = 0
    two_matches = 0
    three_matches = 0
    
    for chair, student in output_list:
        prefs = 0
        if(chair.prefs[0] == student.prefs[0]) or (not student.prefs[0]):
            prefs += 1
        if(chair.prefs[1] == student.prefs[1]) or (not student.prefs[1]):
            prefs += 1
        if(chair.prefs[2] == student.prefs[2]) or (not student.prefs[2]):
            prefs += 1
        preference_matches.append(prefs/3)
        if prefs == 1:
            one_match += 1
        elif prefs == 2:
            two_matches += 1
        elif prefs == 3:
            three_matches += 1

    percents_sum = sum(preference_matches)

    match_percentage = '%.2f'%(((percents_sum/len(preference_matches)) * 100))
    one_match_percentage = '%.2f'%((one_match/percents_sum * 100))
    two_matches_percentage = '%.2f'%((two_matches/percents_sum * 100))
    three_matches_percentage = '%.2f'%((three_matches/percents_sum * 100))
    return_string = "The match percentage for the output is {}, {} percent has 1 match, {} percent has 2 match, {} percent has 3 match".format(match_percentage, one_match_percentage, two_matches_percentage, three_matches_percentage)
 
    print(return_string)
def isChairAssigned(chair, list):
    for x in list:
        if chair.chair_id == x[0].chair_id:
            return True
    return False
def class_distribution(original_chairs, assigned_chairs):
    output_list = [x[0] for x in assigned_chairs]
    assigned = " + "
    not_assigned = " - "
    
    current_row = original_chairs[0].chair_id[0]
    output_str = ""
    
    num_assigned = 0
    num_row_total = 0
    
    for chair in original_chairs:
        row = chair.chair_id[0]
        if row != current_row:
            percentage = (num_assigned/num_row_total) * 100
            output_str += f" [ {percentage:.2f}% ]"
            output_str += "\n"
            current_row = row
            num_assigned = 0
            num_row_total = 0
        symbol = None
        if isChairAssigned(chair,assigned_chairs):
            num_row_total += 1
            num_assigned += 1
            symbol = assigned
        else:
            num_row_total += 1
            symbol =  not_assigned
            
        output_str += " [ {}{} ] ".format(chair.chair_id, symbol) 
    percentage = (num_assigned/num_row_total) * 100
    output_str += f" [ {percentage:.2f}% ]"
    from wsgi import CLASSROOMS_DIR
    from os import path
    with open(path.normpath(CLASSROOMS_DIR + '/class_distribution.txt'), 'w') as output_file:
        output_file.write(output_str)
    
def main(argv):
    '''Takes the input arguments and executes the groupre matching algorithm.'''

    argparser = argparse.ArgumentParser()

    chairs_csv: str = None
    preference_csv: str = None
    roster_csv: str = None
    output_csv: str = None

    # groupre.py -c CHAIRS -s STUDENTS -r ROSTER -o OUTPUT
    argparser.add_argument(
        '-c', '--chairs', help='Chairs input file')
    argparser.add_argument(
        '-s', '--students', help='Students input file')
    argparser.add_argument(
        '-r', '--roster', help='Roster input file')
    argparser.add_argument(
        '-o', '--output', help='Output file')
    argparser.set_defaults(fallback=False, output_csv='output.csv')

    if 'groupre.py' in argv[0]:
        parsed_args = argparser.parse_args()
    else:
        parsed_args = argparser.parse_args(argv)

    chairs_csv: str = parsed_args.chairs
    preference_csv: str = parsed_args.students
    roster_csv: str = parsed_args.roster
    output_csv: str = parsed_args.output

    # file error checking
    if not input_checking(chairs_csv,preference_csv,roster_csv,output_csv):
        return False
    raw_preference_file = get_raw(preference_csv)
    raw_chair_file = get_raw(chairs_csv)
    raw_roster_file = get_raw(roster_csv)
    
    preference_validation(raw_preference_file)
    chair_validation(raw_chair_file)
    roster_validation(raw_roster_file)
    
    chair_list = read_chair_file(raw_chair_file)
    student_list , student_tupel_list = read_preference_file(raw_preference_file)
    roster_list, consent_map = read_roster_file(raw_roster_file)

    # merging roster and student list
    students_not_in_prefs = roster_list.difference(student_tupel_list)
    # check if some student not in roster
    not_in_roster_error(student_tupel_list,roster_list)
    # adding consent check to the students
    no_prefs_student_list = []
    for student in students_not_in_prefs:
        s = Student(student[0],student[1], "FALSE", "")
        s.consented = consent_map[s.student_id]
        no_prefs_student_list.append(s)    
    for student in student_list:
        student.consented = consent_map[student.student_id]
    
    # placing students
    newPairs = placeStudents(student_list, chair_list, no_prefs_student_list)

    write_output(output_csv,newPairs)
    metric(newPairs)
    class_distribution(chair_list,newPairs)
if __name__ == '__main__':
    # Benchmark timer start.
    past_time = time.time()
    print('----------')
    
    main(sys.argv)

    # Benchmark timer end.
    print(time.time() - past_time, 'seconds elapsed.')
    print('----------')
