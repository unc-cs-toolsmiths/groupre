import random
import math
from seat_assignment_helpers import *

# removes broken chairs from list
def remove_broken_chairs(available_chairs):
    return [chair for chair in available_chairs if not chair.is_broken]

# removes taken chairs
def remove_taken_chairs(available_unbroken_chairs):
	return [chair for chair in available_unbroken_chairs if not chair.taken]

# removes students already assigned a chair
def remove_taken_students(students):
	return [student for student in students if not student.taken]

# Assigns VIPs to seats with their exact preferences
def assign_VIPs_exactly(vips, available_unbroken_chairs):
    exact_vip_pairs = []
    for vip in vips:
        for chair in available_unbroken_chairs:
            if chair.taken == True:
                continue
            if vip.prefs == chair.prefs:
                if (vip.consented and (chair.consented == 1)):
                    exact_vip_pairs.append([chair, vip])
                    vip.taken = True
                    chair.taken = True
                    break
                elif (vip.consented and (chair.consented == -1)):
                    findConsentGroupChairs(chair.group_id,available_unbroken_chairs)
                    exact_vip_pairs.append([chair, vip])
                    vip.taken = True
                    chair.taken = True                    
                    break
                elif (not vip.consented and (chair.consented == 0)):
                    exact_vip_pairs.append([chair, vip])
                    vip.taken = True
                    chair.taken = True
                    break
                elif (not vip.consented and (chair.consented == -1)):
                    findNoneConsentGroupChairs(chair.group_id,available_unbroken_chairs)
                    exact_vip_pairs.append([chair, vip])
                    vip.taken = True
                    chair.taken = True
                    break
    return exact_vip_pairs

# assign vips to chairs that approximately fit their preferences
def assign_VIPs_approximately(unmatched_vips, available_unbroken_chairs):
    approx_vip_pairs = []
    for v in unmatched_vips:
        min_val = 999
        minC = None
        #run it through once and match approx seats
        if v.consented:
            for c in available_unbroken_chairs:
                if not c.taken and not c.is_broken and (c.consented == -1 or c.consented == 1):
                    temp_min = findApprox(c,v)
                    if temp_min < min_val:
                        min_val = temp_min
                        minC = c
            if (minC.consented == -1):
                findConsentGroupChairs(minC.group_id,available_unbroken_chairs)
            approx_vip_pairs.append([minC,v])
            minC.taken = True
            v.taken = True
        else:
            for c in available_unbroken_chairs:
                if not c.taken and not c.is_broken and (c.consented == -1 or c.consented == 0):
                    temp_min = findApprox(c,v)
                    if temp_min < min_val:
                        min_val = temp_min
                        minC = c
            # new fall back appproach for consent feature
            if (minC.consented == -1):
                findNoneConsentGroupChairs(minC.group_id,available_unbroken_chairs)
            approx_vip_pairs.append([minC,v])
            minC.taken = True
            v.taken = True
    return approx_vip_pairs

# assign non vips to seats witht their exact preferences
def assign_non_VIPs_exactly(unmatched_non_vips, available_unbroken_chairs):
    exact_non_vip_pairs = []
    # find perfect match in preference list
    for non_vips in unmatched_non_vips:
        for chair in available_unbroken_chairs:
            if chair.taken == True:
                continue
            if non_vips.prefs == chair.prefs:
                if (non_vips.consented and (chair.consented == 1)):
                    exact_non_vip_pairs.append([chair,non_vips])
                    non_vips.taken = True
                    chair.taken = True
                    break
                elif (non_vips.consented and (chair.consented == -1)):
                    findConsentGroupChairs(chair.group_id,available_unbroken_chairs)
                    exact_non_vip_pairs.append([chair,non_vips])
                    non_vips.taken = True
                    chair.taken = True                    
                    break
                elif (not non_vips.consented and (chair.consented == 0)):
                    exact_non_vip_pairs.append([chair,non_vips])
                    non_vips.taken = True
                    chair.taken = True
                    break
                elif (not non_vips.consented and (chair.consented == -1)):
                    findNoneConsentGroupChairs(chair.group_id,available_unbroken_chairs)
                    exact_non_vip_pairs.append([chair,non_vips])
                    non_vips.taken = True
                    chair.taken = True
                    break
    return exact_non_vip_pairs

# assign non vips to seats with approximately their preferences
def assign_non_VIPs_approximately(unmatched_non_vips, available_unbroken_chairs):
    approx_non_vip_pairs = []
    # find approximate match
    for s in unmatched_non_vips:
        min_val = 999
        minC = None
        if s.consented:
            for c in available_unbroken_chairs:
                if (not c.taken) and (not c.is_broken) and (c.consented == -1 or c.consented == 1):
                    temp_min = findApprox(c,s)
                    if temp_min < min_val:
                        min_val = temp_min
                        minC = c
            if (minC.consented == -1):
                findConsentGroupChairs(minC.group_id,available_unbroken_chairs)
            approx_non_vip_pairs.append([minC,s])
            minC.taken = True
            s.taken = True
        else:
            for c in available_unbroken_chairs:
                if not c.taken and not c.is_broken and (c.consented == -1 or c.consented == 0):
                    temp_min = findApprox(c,s)
                    if temp_min < min_val:
                        min_val = temp_min
                        minC = c
            if (minC.consented == -1):
                findNoneConsentGroupChairs(minC.group_id,available_unbroken_chairs)
            approx_non_vip_pairs.append([minC,s])
            minC.taken = True
            s.taken = True
    return approx_non_vip_pairs

# assign noPrefs to the rest of the open seats
# runtime O(n)
def assign_no_prefs(unmatched_no_prefs, available_unbroken_chairs):
    no_prefs_pairs = []
    for student in unmatched_no_prefs:
        if student.consented:
            for chair in available_unbroken_chairs:
                if (chair.taken):
                    continue
                if (chair.consented == 1):
                    no_prefs_pairs.append([chair,student])
                    chair.taken = True
                    student.taken = True
                    break
            else:
                for chair in available_unbroken_chairs:
                    if (chair.taken):
                        continue
                    if chair.consented == -1:
                        findConsentGroupChairs(chair.group_id,available_unbroken_chairs)
                        no_prefs_pairs.append([chair,student])
                        chair.taken = True
                        student.taken = True
                        break
        else:
            for chair in available_unbroken_chairs:
                if (chair.taken):
                    continue
                if (chair.consented == 0):
                    no_prefs_pairs.append([chair,student])
                    chair.taken = True
                    student.taken = True
                    break
            else:
                for chair in available_unbroken_chairs:
                    if (chair.taken):
                        continue
                    if chair.consented == -1:
                        findNoneConsentGroupChairs(chair.group_id,available_unbroken_chairs)
                        no_prefs_pairs.append([chair,student])
                        chair.taken = True
                        student.taken = True
                        break

    return no_prefs_pairs

# place students into matching/prefered seats
def placeStudents(student_list, chair_list, students_not_in_prefs):
    pairs = []
    VIPs = []
    nonVIPs = []
    noPrefs = students_not_in_prefs
    # Make a seperate function
    # split students up into 3 categories
    # randomize the students before assigning them to lists
    random.shuffle(student_list)
    for student in student_list:
        if student.is_VIP or (student.prefs[2] == 1.1):
            VIPs.append(student)
        else:
            noPref = True
            for x in student.prefs:
                if x != 0:
                    noPref = False
            if noPref:
                noPrefs.append(student)
            else:
                nonVIPs.append(student)
    # reshuffle no prefs after appending the students with no response
    random.shuffle(noPrefs)
    
    # sort each categories
    sortByPrefs(VIPs)
    sortByPrefs(nonVIPs)
    # sortByPrefs(chair_list)

    # remove broken chairs from chairs list
    available_chair_list = remove_broken_chairs(chair_list)

    c_len = len(available_chair_list)
    s_len = len(VIPs) + len(nonVIPs) + len(noPrefs)
    if c_len < s_len:
        raise NameError("Amount of students {} greater than amount of chairs {}".format(s_len,c_len))
    # find perfect match in VIP list as first priority
    exact_vip_pairs = assign_VIPs_exactly(VIPs, available_chair_list)

    # find close approx. seat for VIP
    available_chair_list = remove_taken_chairs(available_chair_list)
    unmatched_vips = remove_taken_students(VIPs)
    approx_vip_pairs = assign_VIPs_approximately(unmatched_vips, available_chair_list)

    # find perfect match in preference list
    available_chair_list = remove_taken_chairs(available_chair_list)
    exact_non_vip_pairs = assign_non_VIPs_exactly(nonVIPs, available_chair_list)

    # find approximate match
    available_chair_list = remove_taken_chairs(available_chair_list)
    unmatched_non_vips = remove_taken_students(nonVIPs)
    approx_non_vip_pairs = assign_non_VIPs_approximately(unmatched_non_vips, available_chair_list)

    # fill the rest of unmatched seats
    available_chair_list = remove_taken_chairs(available_chair_list)
    no_prefs_pairs = assign_no_prefs(noPrefs, available_chair_list)

    pairs = exact_vip_pairs + approx_vip_pairs + exact_non_vip_pairs + approx_non_vip_pairs + no_prefs_pairs

    pairs.sort(key=lambda x: x[0].chair_id[0])

    return pairs
