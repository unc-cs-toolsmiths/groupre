# find the seat difference
def findApprox(chair, student):
    diff = 0
    for x in range(len(student.prefs)):
        if x == 2:
            if not student.prefs[x]:
                continue
            else:
                diff += abs(student.prefs[x] - chair.prefs[x])
        else:
            diff += abs(student.prefs[x] - chair.prefs[x])
    return diff

def findConsentGroupChairs(groupID,chairs):
    for c in chairs:
        if c.group_id == groupID:
            c.consented = 1

def findNoneConsentGroupChairs(groupID,chairs):
    for c in chairs:
        if c.group_id == groupID:
            c.consented = 0
def convert_pref(front_back,section,left,aisle):
    result = ""
    if front_back == "Front row":
        result = result+"f,"
    elif front_back == "Anywhere in the first seven front rows":
        result = result+"fi,"
    elif front_back == "The last row possible (NOTE: this may be around row N, and not the last row of the classroom)":
        result = result+"b,"
    elif front_back == "Towards the back of the classroom":
        result = result+"bi,"
    elif front_back == "No preference for location in the room":
        pass 
    elif front_back == "":
        pass
    else:
        raise ValueError(front_back + " not in preference selection.")
    if section == "The large middle section":
        result = result+"middle,"
    elif section == "The left hand section (when I'm facing the screens)":
        result = result+"left,"
    elif section == "The right hand section (when I'm facing the screens)":
        result = result+"right,"
    elif section == "No preference for region of the classroom":
        pass
    elif section == "":
        pass
    else:
        raise ValueError(section +  " not in preference selection.")
    if left == "Yes":
        result = result+"la"
    elif left == "No":
        if aisle == "Yes":
            result = result+"a"
        elif aisle == "No":
            pass
        elif aisle == "":
            pass
        else:
            raise ValueError(aisle+" not in preference selection.")
    elif left == "":
        pass    
    else:
        raise ValueError(left + " not in preference selection.")
    return result.split(",")
# O(nlogn) sorting run time
def sortByPrefs(arr):
    arr.sort(key=lambda x:x.numPref,reverse = True)