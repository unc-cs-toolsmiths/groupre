import unittest
import sys
from pathlib import Path
groupre_path = str(Path().resolve().parent)
sys.path.append(groupre_path)
from src.data_structures.chair import Chair
'''
Testing all functions in chair.py file

use "python3 -m unittest filename" to run the test 
'''
class Test_Chair(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test_Chair, self).__init__(*args, **kwargs)
        self.chair1 = Chair("a2","2",["la","b","", "right"])
        self.chair2 = Chair("b4", "6",["f","left","la"])
        self.chair3 = Chair("c6", "9", ["br","","la"])
    def test_preference_count(self):
        self.assertEqual(self.chair1.numPref,3)
        self.assertEqual(self.chair2.numPref,3)
        self.assertEqual(self.chair3.numPref,1)
    def test_broken(self):
        self.assertEqual(self.chair1.is_broken,0)
        self.assertEqual(self.chair2.is_broken,0)
        self.assertEqual(self.chair3.is_broken,1)
    def test_PI(self):
        self.assertEqual(self.chair1.chair_id,"a2")
        self.assertEqual(self.chair2.chair_id,"b4")
        self.assertEqual(self.chair3.chair_id,"c6")
        self.assertEqual(self.chair1.group_id,"2")
        self.assertEqual(self.chair2.group_id,"6")
        self.assertEqual(self.chair3.group_id,"9")
    def test_preference_array(self):
        self.assertEqual(self.chair1.prefs,[17,2.7,1.1])
        self.assertEqual(self.chair2.prefs,[13,0.7,1.1])
        self.assertEqual(self.chair3.prefs,[0,0,1.1])

'''
noticed serveral issues with chair.py
1. no error checking for argument types
2. l doesn't exist anymore and la is the new fav
'''
if __name__ == '__main__':
    unittest.main()