import unittest
import sys
import random
from pathlib import Path
groupre_path = str(Path().resolve().parent)+"/src"
sys.path.append(groupre_path)
from groupre import *
from data_structures.student import Student
from data_structures.chair import Chair
'''
Testing all functions in groupre.py file
When in the unittest directory
use "python3 -m unittest filename" to run the test 
'''
random.seed(30)
class Test_Groupre(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test_Groupre, self).__init__(*args, **kwargs)