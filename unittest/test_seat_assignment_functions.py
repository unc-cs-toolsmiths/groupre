import unittest
import sys
import random
from pathlib import Path
groupre_path = str(Path().resolve().parent)+"/src"
sys.path.append(groupre_path)
from groupre import *
from data_structures.student import Student
from data_structures.chair import Chair
'''
Testing all functions in seat_assignment_functions.py file
When in the unittest directory
use "python3 -m unittest filename" to run the test 
'''
random.seed(30)
class Test_Seat_Assignment_Functions(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test_Seat_Assignment_Functions, self).__init__(*args, **kwargs)
        self.chair1 = Chair("a2","2",["la","b","", "right"])
        self.chair2 = Chair("b4", "6",["f","left","la"])
        self.chair3 = Chair("c6", "9", ["","","la"])
        self.student1 = Student("chase","123456","Yes",["","b","la", ""])
        self.student2 = Student("rob", "67891","",["f","left","la"])
        self.student3 = Student("andrew", "98109", "Yes", ["hello","","la"])
        self.chairs = [self.chair1,self.chair2,self.chair3]
        self.students = [self.student1,self.student2,self.student3]
   
    def test_remove_broken_chairs(self):
        broken_chair = Chair("g9","2",["la","br","", "right"])
        self.assertEqual(remove_broken_chairs([self.chair1,self.chair2,self.chair3,broken_chair]),[self.chair1,self.chair2,self.chair3])
    def test_remove_taken_chairs(self):
        taken_chair = Chair("g9","2",["la","", "right"])
        taken_chair.taken = True
        self.assertEqual(remove_taken_chairs([self.chair1,self.chair2,self.chair3,taken_chair]),self.chairs)
    def test_remove_taken_students(self):
        taken_student = Student("mia","123125621","TRUE",["","b","la", ""])
        taken_student.taken = True
        self.assertEqual(remove_taken_students([self.student1,self.student2,self.student3,taken_student]),self.students)
    def test_assign_VIPs_exactly(self):
        VIP1 = Student("chase","123456","TRUE",["right","b","la", ""])
        chair1 = Chair("a2","2",["la","b","", "right"])
        chair2 = Chair("a3","4",["la","b","", "right"])
        self.assertEqual(assign_VIPs_exactly([VIP1],[chair1,chair2])[0],[chair1,VIP1])
                
        VIP1.taken = False
        chair1.taken = False
        chair2.taken = False
                
        VIP1.consented = 1
        chair1.consented = 0
        chair2.consented = 1
        
        self.assertEqual(assign_VIPs_exactly([VIP1],[chair1,chair2])[0],[chair2,VIP1])

        VIP1.taken = False
        chair1.taken = False
        chair2.taken = False
        
        self.assertNotEqual(assign_VIPs_exactly([VIP1],[chair1,chair2])[0],[chair1,VIP1])

    def test_assign_VIPs_approximately(self):
        VIP1 = Student("chase","123456","TRUE",["","b","la", ""])
        chair1 = Chair("a3","4",["","b","", ""])
        chair2 = Chair("a4","5",["","b","", ""])
        self.assertEqual(assign_VIPs_approximately([VIP1],[chair1,chair2])[0],[chair1,VIP1])
        
        VIP1.taken = False
        chair1.taken = False
        chair2.taken = False
        
        VIP1.consented = 1
        chair1.consented = 0
        chair2.consented = 1
        self.assertEqual(assign_VIPs_approximately([VIP1],[chair1,chair2])[0],[chair2,VIP1])

        VIP1.taken = False
        chair1.taken = False
        chair2.taken = False
        
        self.assertNotEqual(assign_VIPs_approximately([VIP1],[chair1,chair2])[0],[chair1,VIP1])
    def test_assign_non_VIPs_exactly(self):
        non_VIP = Student("mia","894953","FALSE",["la","b","", "right"])
        chair1 = Chair("a2","2",["la","b","", "right"])
        chair2 = Chair("a3","4",["la","b","", "right"])
        self.assertEqual(assign_non_VIPs_exactly([non_VIP],[chair1,chair2])[0],[chair1,non_VIP])
        
                 
        non_VIP.taken = False
        chair1.taken = False
        chair2.taken = False
                
        non_VIP.consented = 1
        chair1.consented = 0
        chair2.consented = 1
        
        self.assertEqual(assign_non_VIPs_exactly([non_VIP],[chair1,chair2])[0],[chair2,non_VIP])
        
        non_VIP.taken = False
        chair1.taken = False
        chair2.taken = False
        
        self.assertNotEqual(assign_non_VIPs_exactly([non_VIP],[chair1,chair2])[0],[chair1,non_VIP])
    def test_assign_non_VIPs_approximately(self):
        non_VIP = Student("mia","894953","FALSE",["f","a"])
        chair1 = Chair("a2","2",["la","b","", "right"])
        chair2 = Chair("a3","4",["","la","f", ""])
        self.assertEqual(assign_non_VIPs_approximately([non_VIP],[chair1,chair2])[0],[chair2,non_VIP])
        
        non_VIP.taken = False
        chair1.taken = False
        chair2.taken = False
                
        non_VIP.consented = 1
        chair1.consented = 1
        chair2.consented = 0
        
        self.assertEqual(assign_non_VIPs_approximately([non_VIP],[chair1,chair2])[0],[chair1,non_VIP])
        
        non_VIP.taken = False
        chair1.taken = False
        chair2.taken = False
        
        self.assertNotEqual(assign_non_VIPs_approximately([non_VIP],[chair1,chair2])[0],[chair2,non_VIP])
    def test_assign_no_prefs(self):
        student1 = Student("mia","894953","FALSE",["",""])
        chair1 = Chair("a2","2",["la","b","", "right"])
        student2 = Student("mias","894953","FALSE",["",""])
        chair2 = Chair("a3","2",["la","b","", "right"])
        student3 = Student("miad","894953","FALSE",["",""])
        chair3 = Chair("a4","4",["la","b","", "right"])
        
        student1.consented = 1
        student2.consented = 0
        student3.consented = 1
        
        result = assign_no_prefs([student1,student2,student3],[chair1,chair2,chair3])
        self.assertEqual(result[0],[chair1,student1])
        self.assertEqual(result[1],[chair3,student2])
        self.assertEqual(result[2],[chair2,student3])

    def test_placeStudents(self):
        VIP1 = Student("hello","123456","Yes",["right","b","la", ""])
        VIP2 = Student("chase","98109","Yes",["","bi","la", ""])
        non_VIP1 = Student("bob","894453","No",["f","a"])
        non_VIP2 = Student("mia","124953","No",["fi","a"])
        no_pref = Student("john","3125213","No",[""])
        chair_VIP = Chair("a2","2",["la","b","", "right"])
        chair_VIP_approx = Chair("a3","2",["la","b","", ""])
        chair_non_VIP = Chair("a4","2",["f","a","", ""])
        chair_non_VIP_approx = Chair("a2","2",["fi","la","", ""])
        chair_none = Chair("a5","2",["","","", ""])
        #for no extra roster seats
        result = placeStudents([VIP1,VIP2,non_VIP1,non_VIP2,no_pref],[chair_VIP,chair_VIP_approx,chair_non_VIP,chair_non_VIP_approx,chair_none],[])
        #for extra roster seats
        self.assertEqual(result,[[chair_VIP,VIP1],[chair_VIP_approx,VIP2],[chair_non_VIP,non_VIP1],[chair_non_VIP_approx,non_VIP2],[chair_none,no_pref]])
    def test_placeStudents_with_roster(self):
        VIP1 = Student("hello","123456","Yes",["right","b","la", ""])
        VIP2 = Student("chase","98109","Yes",["","bi","la", ""])
        non_VIP1 = Student("bob","894453","No",["f","a"])
        non_VIP2 = Student("mia","124953314","No",["fi","a"])
        no_pref = Student("john","312521312","No",[""])
        no_in_pref = Student("chaseses","123456789","No",[""])
        chair_VIP = Chair("a2","2",["la","b","", "right"])
        chair_VIP_approx = Chair("a3","2",["la","b","", ""])
        chair_non_VIP = Chair("a4","2",["f","a","", ""])
        chair_non_VIP_approx = Chair("a2","2",["fi","la","", ""])
        chair_none = Chair("a5","2",["","","", ""])
        
        result = placeStudents([VIP1,VIP2,non_VIP1,non_VIP2],[chair_VIP,chair_VIP_approx,chair_non_VIP,chair_non_VIP_approx,chair_none],[no_in_pref])
        self.assertEqual(result[-1][1].onyen,"chaseses")
        self.assertEqual(result[-1][0].chair_id,"a5")

if __name__ == '__main__':
    unittest.main()