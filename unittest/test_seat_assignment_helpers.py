import unittest
import sys
import random
from pathlib import Path
groupre_path = str(Path().resolve().parent)+"/src"
sys.path.append(groupre_path)
from seat_assignment_functions import *
from data_structures.student import Student
from data_structures.chair import Chair
'''
Testing all functions in seat_assignment_helpers.py file
When in the unittest directory
use "python3 -m unittest filename" to run the test 
'''
random.seed(30)

class Test_Seat_Assignment_Helpers(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test_Seat_Assignment_Helpers, self).__init__(*args, **kwargs)
        self.chair1 = Chair("a2","2",["la","b","", "right"])
        self.chair2 = Chair("b4", "2",["f","left","la"])
        self.chair3 = Chair("c6", "9", ["","","la"])
        self.student1 = Student("chase","123456","Yes",["","b","la", ""])
        self.student2 = Student("rob", "67891","",["f","left","la"])
        self.student3 = Student("andrew", "98109", "Yes", ["hello","","la"])
        self.chairs = [self.chair1,self.chair2,self.chair3]
        self.students = [self.student1,self.student2,self.student3]
        
    def test_findApprox(self):
        self.assertEqual(findApprox(self.chair1,self.student1),17)
        self.assertEqual(findApprox(self.chair2,self.student2),0)
        self.assertEqual(findApprox(self.chair3,self.student3),0)
    def test_findConsentGroupChairs(self):
        findConsentGroupChairs("2",[self.chair1,self.chair2,self.chair3])
        self.assertEqual(self.chair1.consented, 1)
        self.assertEqual(self.chair2.consented, 1)  
    def test_findNoneConsentGroupChairs(self):
        findNoneConsentGroupChairs("2",[self.chair1,self.chair2,self.chair3])
        self.assertEqual(self.chair1.consented, 0)
        self.assertEqual(self.chair2.consented, 0)  
    def test_sortByPrefs(self):
        student_sorted = [Student("rob", "67891","FALSE",["f","left","la"]),Student("chase","123456","TRUE",["","b","la", ""]),Student("andrew", "98109", "TRUE", ["hello","","la"])]
        student_unsorted = [Student("chase","123456","TRUE",["","b","la", ""]),Student("andrew", "98109", "TRUE", ["hello","","la"]),Student("rob", "67891","FALSE",["f","left","la"])]
        sortByPrefs(student_unsorted)
        self.assertEqual(student_sorted[0].student_id,student_unsorted[0].student_id)
        self.assertEqual(student_sorted[1].student_id,student_unsorted[1].student_id)
        self.assertEqual(student_sorted[2].student_id,student_unsorted[2].student_id)
        
    def test_convert_pref(self):
        result = convert_pref("Front row","The large middle section","Yes","Yes")
        self.assertEqual(result,["f","middle","la"])
if __name__ == '__main__':
    unittest.main()