import unittest
import sys
from pathlib import Path
groupre_path = str(Path().resolve().parent)
sys.path.append(groupre_path)
from src.data_structures.student import Student
'''
Testing all functions in student.py file

use "python3 -m unittest filename" to run the test 
'''
class Test_Student(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test_Student, self).__init__(*args, **kwargs)
        self.student1 = Student("chase","123456","Yes",["","b","", ""])
        self.student2 = Student("rob","67891","",["f","left","la"])
        self.student3 = Student("andrew","98109", "Yes", ["hello","","la"])
    def test_preference_count(self):
        self.assertEqual(self.student1.numPref,1)
        self.assertEqual(self.student2.numPref,3)
        self.assertEqual(self.student3.numPref,1)
    def test_VIP(self):
        self.assertEqual(self.student1.is_VIP,True)
        self.assertEqual(self.student2.is_VIP,False)
        self.assertEqual(self.student3.is_VIP,True)
    def test_PID(self):
        self.assertEqual(self.student1.student_id,"123456")
        self.assertEqual(self.student2.student_id,"67891")
        self.assertEqual(self.student3.student_id,"98109")
        self.assertEqual(self.student1.onyen,"chase")
        self.assertEqual(self.student2.onyen,"rob")
        self.assertEqual(self.student3.onyen,"andrew")
    def test_preference_array(self):
        self.assertEqual(self.student1.prefs,[0,2.7,0])
        self.assertEqual(self.student2.prefs,[13,0.7,1.1])
        self.assertEqual(self.student3.prefs,[0,0,1.1])

'''
noticed serveral issues with student.py
1. no error checking for argument types
2. l doesn't exist anymore and la is the new fav
'''
if __name__ == '__main__':
    unittest.main()