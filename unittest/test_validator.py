import unittest
import sys
from pathlib import Path
groupre_path = str(Path().resolve().parent)
sys.path.append(groupre_path)
from src.data_structures.validator import Validator
'''
Testing all functions in validator.py file

use "python3 -m unittest filename" to run the test 
'''
class Test_Validator(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(Test_Validator, self).__init__(*args, **kwargs)
        self.preference_correct = ["","first","Emma","onyen","847362748","Anywhere in the first seven front rows","The large middle section","No",	"No", "No comment.","YES"]
        self.preference_incorrect = ["","first","Emma","onyen","8436274","Wrong, Anywhere in the first seven front rows","The large middle section","No",	"No", "No comment.","something else"]
        self.chair_correct = ["B2","2","f","la","right"]
        self.chair_incorrect = ["B2","2","g","la","right"]
        self.roster_correct = ["123456789","Joe","joe@live.unc.edu","1"]
        self.roster_incorrect = ["1234567","Joe","joelive.unc.edu",""]
        self.student_validator = Validator("student")
        self.chair_validator = Validator("chair")
        self.roster_validator = Validator("roster")
    def test_check_header(self):
        student_header_correct = ["Timestamp", "What is your last name?", "What is your first name?","What is your UNC onyen?", "Enter your UNC PID (enter a 7 digit number)", "Which row is important to you? If we are unable to accommodate a specific request, we will place you as close to your requested location as possible.", 
                                       "When facing the screens, what region of the classroom is important to you?", "Do you require a left handed desk?", "Do you need to sit in an aisle seat?", "Is there anything else you want to tell me about your seating request? (e.g. related to your ARS accommodation or other personal matter)","VIP"]
        student_header_incorrect = ["Timestamp", "What is your last name?", "What is your first name?","What is your UNC onyen incorrect?", "Enter your UNC PID (enter a 7 digit number)", "Which row is important to you? If we are unable to accommodate a specific request, we will place you as close to your requested location as possible.", 
                                       "When facing the screens, what region of the classroom is important to you?", "Do you require a left handed desk?", "Do you need to sit in an aisle seat?", "Is there anything else you want to tell me about your seating request? (e.g. related to your ARS accommodation or other personal matter)","VIP"]
        chair_header_correct = ["CID","TeamID","Preferences"]
        chair_header_incorrect = ["CID extrac","TeamID","Preferences"]
        roster_header_correct = ["ID","Name","Email"]
        roster_header_incorrect = ["IDD","Name","Email"]
        
        try:
            self.student_validator.check_header(student_header_correct)
            self.chair_validator.check_header(chair_header_correct)
            self.roster_validator.check_header(roster_header_correct)
        except:
            self.fail("failed test_check_header")
        
        with self.assertRaises(NameError): self.student_validator.check_header(student_header_incorrect)
        with self.assertRaises(NameError): self.chair_validator.check_header(chair_header_incorrect) 
        with self.assertRaises(NameError): self.roster_validator.check_header(roster_header_incorrect)

    def test_check_consent(self):
        try:
            self.roster_validator.check_consent(self.roster_correct)
        except:
            self.fail("failed test_check_consent")
        with self.assertRaises(ValueError): self.student_validator.check_consent(self.roster_incorrect)
    def test_check_PID(self):
        try:
            self.student_validator.check_PID(self.preference_correct)
            self.roster_validator.check_PID(self.roster_correct)
        except:
            self.fail("failed test_check_PID")
            
        with self.assertRaises(ValueError): self.student_validator.check_PID(self.preference_incorrect)
        with self.assertRaises(ValueError): self.roster_validator.check_PID(self.roster_incorrect)

    def test_check_VIP(self):
        try:
            self.student_validator.check_VIP(self.preference_correct)
        except:
            self.fail("failed test_check_VIP")
        with self.assertRaises(ValueError): self.student_validator.check_VIP(self.preference_incorrect)
    def test_check_email(self):
        try:
            self.roster_validator.check_email(self.roster_correct)
        except:
            self.fail("failed test_check_email")
        with self.assertRaises(ValueError): self.roster_validator.check_email(self.roster_incorrect)

    def test_check_chair_preferences(self):
        try:
            self.chair_validator.check_chair_preferences(self.chair_correct)
        except:
            self.fail("failed test_check_VIP")
        with self.assertRaises(ValueError): self.chair_validator.check_chair_preferences(self.chair_incorrect)

    def test_check_student_preferences(self):
        try:
            self.student_validator.check_student_preferences(self.preference_correct)
        except:
            self.fail("failed test_check_VIP")
        with self.assertRaises(ValueError): self.student_validator.check_student_preferences(self.preference_incorrect)

    