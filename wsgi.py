'''Website module.'''
'''
wsgi file is used to handle requests from the frontend javascript
'''
import os
import json
from flask import (Flask, jsonify)
import firebase_admin
#import pyrebase


# Define directory
temp_file = os.getcwd()

firebase_key = json.loads(os.environ['FIREBASE_KEY']) if 'FIREBASE_KEY' in os.environ else os.path.normpath(temp_file+'/key.json')
firebase_fbconfig = json.loads(os.environ['FIREBASE_CONFIG']) if 'FIREBASE_CONFIG' in os.environ else json.load(open(os.path.normpath(temp_file+'/fbconfig.json')))

UPLOAD_FOLDER = os.path.normpath(temp_file + '/uploads/')


ALLOWED_EXTENSIONS = set(['csv'])
if os.path.exists('/chairs'):
    CHAIRS_DIR = '/chairs/'
else:
    CHAIRS_DIR = os.path.normpath(UPLOAD_FOLDER + '/chairs/')

if os.path.exists('/classrooms'):
    CLASSROOMS_DIR = '/classrooms/'
else:
    CLASSROOMS_DIR = os.path.normpath(UPLOAD_FOLDER + '/classrooms/')


application = Flask(__name__)
app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 5 * 1024 * 1024
application.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'
application.config['CHAIRS_DIR'] = CHAIRS_DIR
application.config['CLASSROOMS_DIR'] = CLASSROOMS_DIR


cred = firebase_admin.credentials.Certificate(firebase_key)
#pb = pyrebase.initialize_app(firebase_fbconfig)

#auth = pb.auth() #This variable will allow you to register/login users
#db = pb.database() #This variable allows you to retrieve/set data from/for the database.




#helper scripts
# returns response based on flag code
def response(status = 200):
    resp = jsonify(success=True)
    resp.status_code = status
    return resp


def run_test(students, row_count):
    main_dir = os.path.dirname(os.path.realpath(__file__))
    test_location = os.path.join(main_dir, '/uploads/')
    chairs = os.path.join(main_dir, 'src/python/chairs/')
    return run_groupre(students, chairs)

def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())
def json2obj(data):
    return json.loads(data, object_hook=_json_object_hook)


application.secret_key = '5791628bb0b13ce0c676dfde280ba245'

from groupre_core_pages import saveClass, saveRoom, edit_team, create_team, create_room, changeTemplate, retrieve_class, retrieve_team, retrieve_file, downloadcsv, selectRoom, downloadChair, upload_file, run_groupre_program, docs
application.add_url_rule('/class-saver', 'class-saver', saveClass, methods=['POST'])
application.add_url_rule('/room-saver', 'room-saver', saveRoom, methods=['GET', 'POST'])
application.add_url_rule('/team-edition', 'team-edition', edit_team)
application.add_url_rule('/team-creation', 'team-creation', create_team)
application.add_url_rule('/editTemplate', 'editTemplate', changeTemplate)
application.add_url_rule('/room-creation', 'room-creation', create_room)
application.add_url_rule('/class/<string:jsonName>', 'class', view_func=retrieve_class, methods=['GET', 'POST'])
application.add_url_rule('/chair/<string:jsonName>', 'chair', retrieve_team, methods=['GET', 'POST'])
application.add_url_rule('/template/<string:jsonName>', 'template', retrieve_file, methods=['GET', 'POST'])
application.add_url_rule('/download/<string:output_name>', 'download', downloadcsv, methods=['GET', 'POST'])
application.add_url_rule('/room-select', 'room-select', selectRoom)
application.add_url_rule('/download-chair/<output_name>', 'download-chair', downloadChair, methods=['GET', 'POST'])
application.add_url_rule('/upload/<string:roomID>', 'upload', upload_file, methods=['GET', 'POST'])
application.add_url_rule('/', 'Home', run_groupre_program, methods=['GET', 'POST'])
application.add_url_rule('/docs/', 'docs', docs)

from registration import sign_up, login, student_form, account_page
application.add_url_rule('/sign-up', 'sign-up', sign_up, methods=['GET', 'POST'])
application.add_url_rule('/login/', 'login', login, methods=['GET', 'POST'])
application.add_url_rule('/student_form', 'student-form', student_form, methods=['GET', 'POST'])
application.add_url_rule('/account', 'account', account_page, methods=['GET'])

if 'FLASK_ENV' in os.environ and os.environ['FLASK_ENV']=="development":
    application.run(debug=True)